Ansible role: pipenv
=========

![pipeline](https://gitlab.com/tcblome/ansible-role-pipenv/badges/master/build.svg)

This role installs [pipenv](https://packaging.python.org/key_projects/#pipenv/).


Requirements
------------
git

Installation
------------

Using `ansible-galaxy`:

```shell
$ ansible-galaxy install https://gitlab.com/tcblome/ansible-role-pipenv.git
```

Using `requirements.yml`:
```yaml
 src: git+https://gitlab.com/tcblome/ansible-role-pipenv.git
 name: tcblome.pipenv
```
Using `git`:
```shell
$ git clone https://gitlab.com/tcblome/ansible-role-pipenv.git tcblome.pipenv
```

Role Variables
--------------
```yaml

# defaults file for ansible-role-pipenv
#
# pipenv_version	*	# version of pipenv to install

```

Dependencies
------------

This role can be used independently.

Example Playbook
----------------

Sample :
```
    - hosts: servers
      roles:
        - { role: tcblome.pipenv,
	         pipenv_version: "2018.7.1"
	        }
```

License
------------------
[LICENSE](LICENSE)

Author Information
------------------

This role is published for private use by @tcblome
